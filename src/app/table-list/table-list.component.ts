import { Component, OnInit } from '@angular/core';
import {LoadDataService} from "../services/load-data.service";
import {Router} from "@angular/router";
import {ExcelService} from "../services/excel.service";
import {LoginService} from '../services/login.service';
declare var $: any;


@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

  p: number = 1;

  fileName= 'ExcelSheet.xlsx';

  listpotentiel = [1, 2, 3];

  listimpotance = ['A', 'B', 'C', 'D'];

  listTypeclient = ['ONG', 'ASSOCIATION', 'SOCIETE PRIVE', 'ORGANISATION PUBLIC', 'PARTICULIER', 'COOPERATIVE', 'SOCIETE ENTRANGERE', 'SALON', 'EVENEMENT'];

  listdommaine = ['INTRANTS AGRICOLES',
    'INTRANTS HYDRAULIQUES',
    'MEDIAS COMMUNICATION',
    'AGRO-INDUSTRIE',
    'COUVOIIR',
    'FERME',
    'EXPLOITATION AGRICOLE',
    'IMPORTATEUR AGRICOLE',
    'DISTRIBUTION',
    'COMMERCANTS EN GROS',
    'COMMERCANTS DETAILS',
    'LOGISTIQUES',
    'FORMATIONS',
    'PLUMEUSE',
    'NUTRITION',
    'CUISINE',
    'EVENEMENTIEL',
    'CONSULTANCE'];

  indexofdomaine = 'Domaine';

  indexofimportance = 'Importance';

  indexofpotentiel = 'Potentiel';

  indexoftypeclient = 'TypeClient';

  constructor(public load: LoadDataService, private router: Router, private excelService: ExcelService, public loginService: LoginService) { }

  ngOnInit() {
    this.load.ishowsearch = true;
    this.loginService.loadToken();
  }

  GoToUpdate(info) {
    this.load.clientInstance = info;
    this.load.clientInsert = info;
    console.log(this.load.clientInstance);
    this.router.navigate(['/edit']);
  }
  GoDetails(info){
    this.load.clientInstance=info;
    console.log(this.load.clientInstance);
   this.router.navigate(['/details']);
  }
  remove(info) {
    const cf = confirm('Vous allez supprimer ce client');
    if (cf) {
      this.load.cat.remove(info);
    }
  }

  PrintToCsv() {
    // /* table id is passed over here */
    // let element = document.getElementById('excel-table');
    // const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);
    //
    // /* generate workbook and add the worksheet */
    // const wb: XLSX.WorkBook = XLSX.utils.book_new();
    // XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    //
    // /* save to file */
    // XLSX.writeFile(wb, this.fileName);
    this.excelService.exportAsExcelFile(this.load.cat.data, 'sample');

  }

  loadAll() {
    this.indexoftypeclient = 'TypeClient';
    this.indexofpotentiel = 'Potentiel';
    this.indexofimportance = 'Importance';
    this.indexofdomaine= 'Domaine';
    this.load.loadDB();
  }

  FilterbyTypeclient(tpcl) {
    this.load.loadDB();
    setTimeout(() => {
      let listsort = [];
      this.indexoftypeclient = tpcl;
      console.log(tpcl);
      for (let k = 0; k < this.load.cat.data.length; k++) {
        if (this.load.cat.data[k].typeclient == tpcl) {
          listsort.push(this.load.cat.data[k]);
        }
      }
      this.load.cat.data =listsort;
    }, 1000);
  }

  FilterbyDomaine(doma) {
    console.log(doma);
    this.load.loadDB();
    setTimeout(() => {
      let listsort = [];
      this.indexofdomaine = doma;
      console.log(doma);
      for (let k = 0; k < this.load.cat.data.length; k++) {
        if (this.load.cat.data[k].domaine_activite == doma) {
          listsort.push(this.load.cat.data[k]);
        }
      }
      this.load.cat.data =listsort;
    }, 1000);
  }

  FilterbyImportance(imp) {
    console.log(imp);
    this.load.loadDB();
    setTimeout(() => {
      let listsort = [];
      this.indexofimportance = imp;
      console.log(imp);
      for (let k = 0; k < this.load.cat.data.length; k++) {
        if (this.load.cat.data[k].importance == imp) {
          listsort.push(this.load.cat.data[k]);
        }
      }
      this.load.cat.data =listsort;
    }, 1000);
  }

  FilterbyPotentiel(po) {
    console.log(po);
    this.load.loadDB();
    setTimeout(() => {
      let listsort = [];
      this.indexofpotentiel = po;
      console.log(po);
      for (let k = 0; k < this.load.cat.data.length; k++) {
        if (this.load.cat.data[k].potentiel == po) {
          listsort.push(this.load.cat.data[k]);
        }
      }
      this.load.cat.data =listsort;
    }, 1000);
  }

}
