import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthguardService implements CanActivate {

  canActivate(): Observable<boolean> | Promise<boolean> | boolean  {
    return new Promise(
        (resolve, reject) => {
          if (localStorage.getItem('authToken') != null) {
            resolve(true);
          } else {
            this.router.navigate(['login']);
            resolve(false);
          }
        }
    );
  }


  constructor(private router: Router) { }
}
