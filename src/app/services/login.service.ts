import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
public role = '';
public auth:boolean;
public token:string;
public isconnected = false;
  constructor() { }
 public login(username:string,password:string){
  if((username=='admin' && password=='senagriculture') || (username=='user' && password=='agriculture') ){
    this.auth=true;
    this.role = username;
    localStorage.setItem('role', username);
      this.saveToken();
  }else{
    this.auth=false;
  }
   return this.auth;
 }
 private saveToken(){
   this.token='agri';
   localStorage.setItem('authToken',this.token);
 }
public loadToken(){
  this.token=localStorage.getItem('authToken');
  if(this.token=='agri') {
    this.auth=true;
      this.isconnected = true;
      this.role = localStorage.getItem('role');
  }else{
    this.auth=false;
      this.isconnected = false;
  }
}
 public signOut() {
      localStorage.removeItem('authToken');
      localStorage.removeItem('role');
      this.loadToken();
}

}
