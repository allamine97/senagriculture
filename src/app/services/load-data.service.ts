import { Injectable } from '@angular/core';
import * as Loki from 'lokijs';
import * as LokiIndexedAdapter from 'lokijs/src/loki-indexed-adapter';
@Injectable({
  providedIn: 'root'
})
export class LoadDataService {

  public db: any;
  public cat: any;

  public ishowsearch = false;

  public clientInstance: any = {};

  public searchtext = '';

  public clientInsert = {
    id: '',
    qui: '',
    importance: '',
    potentiel: '',
    nom_entreprise: '',
    segment: '',
    infogen: '',
    source: '',
    statut: '',
    typeclient: '',
    commentaire: [],
    nom_representant: '',
    prenom_representant: '',
    domaine_activite: '',
    telephone_representant: '',
    fonction_representant: '',
    cp_representant: '',
    adresse_representant: '',
    ville_representant: '',
    pays_representant: '',
    date_action: '',
    date_creation: '',
  };

  constructor() { }

  loadDB() {
    const promise = new Promise((resolve, reject) => {
      const adapter = new LokiIndexedAdapter();
      this.db = new Loki('SENAGRI', {
        autosave: true,
        autosaveInterval: 4 * 1000,
        verbose: true,
        adapter: adapter
      });


      this.db.loadDatabase({}, (err) => {
        if (err) {
        }
        else {
          console.log("database loaded.");
          this.cat = this.db.getCollection('client') || this.db.addCollection('client');
          resolve();
        }
      });
    });

    return promise;

  }


  initDB(data) {
    this.cat = this.db.getCollection('client') || this.db.addCollection('client');
    this.cat.insert(data);
    console.log(this.cat.data);
  }

  update(data) {
    this.cat = this.db.getCollection('client') || this.db.addCollection('client');
    this.cat.update(data);
    console.log(this.cat.data);
  }

  LoadData() {

  }

}
