import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'entrefiltre'
})
export class EntrefiltrePipe implements PipeTransform {


  transform(data: any, text: string): any {
    if (text === null || text === '') {
      return data;
    }
    return data.filter(d => d.nom_entreprise.toLowerCase().includes(text.toLowerCase()));

  }

}
