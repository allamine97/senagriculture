import { Component, OnInit } from '@angular/core';
import { LoginService } from 'app/services/login.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;

  constructor(private loginService: LoginService, private router: Router, private formBuilder: FormBuilder, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.initForm();
  }
  initForm(){
    this.loginForm = this.formBuilder.group({
      username: [''],
      password: [''],
    });
  }

  

  Submit(){ 
    let res=this.loginService.login(this.loginForm.value.username,this.loginForm.value.password);
    if(res==true){
      this.loginService.loadToken();
      this.router.navigate(['/table-list'])
      console.log('good');

    }else{
       this.router.navigate(['/login'])
       console.log('erreur auth');
       this.openSnackBar('informations incorrectes', 'statut');
       this.loginForm.reset();
    }
    
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
