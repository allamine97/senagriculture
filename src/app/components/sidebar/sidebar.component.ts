import { Component, OnInit } from '@angular/core';
import {LoadDataService} from "../../services/load-data.service";
import {LoginService} from '../../services/login.service';
import {Router} from '@angular/router';

declare const $: any;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTES: RouteInfo[] = [
    {path: '/table-list', title: 'Clients',  icon:'content_paste', class: '' },
    { path: '/user-profile', title: 'Nouveau Client',  icon:'add', class: '' },
    { path: '/icons', title: 'utilisateurs',  icon:'person', class: '' },
    { path: '/dashboard', title: 'A propos',  icon: 'home', class: '' },

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(public load: LoadDataService,private authS:LoginService,private router:Router) { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;

  };
  logOut(){
      this.authS.signOut();
      this.router.navigate(['/login']);
  }

}
