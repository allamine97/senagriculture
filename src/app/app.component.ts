import { Component, OnInit} from '@angular/core';
import {LoadDataService} from './services/load-data.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private load: LoadDataService) {

  }
  
  ngOnInit(): void {
    this.load.loadDB();
    this.load.LoadData();
  }


}
