import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { DetailsComponent } from 'app/details/details.component';
import {EditComponent} from "../../edit/edit.component";
import { LoginComponent } from 'app/login/login.component';
import { AuthguardService } from 'app/services/authguard.service';

export const AdminLayoutRoutes: Routes = [
    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    { path: 'dashboard',      component: DashboardComponent,canActivate: [AuthguardService] },
    { path: 'user-profile',   component: UserProfileComponent,canActivate: [AuthguardService] },
    { path: 'table-list',     component: TableListComponent,canActivate: [AuthguardService]},
    //{ path: 'typography',     component: TypographyComponent },
    { path: 'icons',          component: IconsComponent,canActivate:[AuthguardService]},
    { path: 'edit',          component: EditComponent,canActivate: [AuthguardService]},
   // { path: 'maps',           component: MapsComponent },
    //{ path: 'notifications',  component: NotificationsComponent },
  //  { path: 'upgrade',        component: UpgradeComponent },
    {path:'login',component:LoginComponent},
    {path:'details',component:DetailsComponent,canActivate: [AuthguardService]}
];
