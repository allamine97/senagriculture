import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {DetailsComponent} from '../../details/details.component';
import {EditComponent} from '../../edit/edit.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {LoginComponent} from '../../login/login.component';
import {NgPrintModule} from 'ng-print';
//import { FlexLayoutModule } from '@angular/flex-layout';
import {
    MatButtonModule,
    MatInputModule,
    MatRippleModule,
    MatFormFieldModule,
    MatSelectModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatBadgeModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatChipsModule,
    MatTooltipModule,
    MatTableModule,
    MatPaginatorModule,
    MatCardModule, MatSnackBarModule

} from '@angular/material';
import {EntrefiltrePipe} from "../../pipes/entrefiltre.pipe";
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminLayoutRoutes),
        FormsModule,
        ReactiveFormsModule,
        MatPaginatorModule,
        NgxPaginationModule,
        MatButtonModule,
        MatRippleModule,
        MatSnackBarModule,
        MatFormFieldModule,
        MatMenuModule,
        MatInputModule,
        MatSelectModule,
        MatTooltipModule,
        MatButtonModule,
        MatInputModule,
        MatRippleModule,
        MatFormFieldModule,
        MatSelectModule,
        MatDatepickerModule,
        MatMenuModule,
        MatToolbarModule,
        MatIconModule,
        MatBadgeModule,
        MatSidenavModule,
        MatListModule,
        MatGridListModule,
        MatRadioModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatChipsModule,
        MatCardModule,
        MatTooltipModule,
        MatTableModule,
        MatCardModule,
        NgPrintModule,
        //FlexLayoutModule,
        MatPaginatorModule
    ],
  declarations: [
    DashboardComponent,
    TableListComponent,
    UserProfileComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    DetailsComponent,
    EditComponent,
      EntrefiltrePipe,
    LoginComponent
  ]
})

export class AdminLayoutModule {}
