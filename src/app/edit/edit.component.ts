import { Component, OnInit } from '@angular/core';
import {LoadDataService} from "../services/load-data.service";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  isReadyTopersonne = false;

  clientForm: FormGroup;

  listpotentiel = [1, 2, 3];

  listimpotance = ['A', 'B', 'C', 'D'];

  listTypeclient = ['ONG', 'ASSOCIATION', 'SOCIETE PRIVE', 'ORGANISATION PUBLIC', 'PARTICULIER', 'COOPERATIVE', 'SOCIETE ENTRANGERE', 'SALON', 'EVENEMENT'];

  listdommaine = ['INTRANTS AGRICOLES',
    'INTRANTS HYDRAULIQUES',
    'MEDIAS COMMUNICATION',
    'AGRO-INDUSTRIE',
    'COUVOIIR',
    'FERME',
    'EXPLOITATION AGRICOLE',
    'IMPORTATEUR AGRICOLE',
    'DISTRIBUTION',
    'COMMERCANTS EN GROS',
    'COMMERCANTS DETAILS',
    'LOGISTIQUES',
    'FORMATIONS',
    'PLUMEUSE',
    'NUTRITION',
    'CUISINE',
    'EVENEMENTIEL',
    'CONSULTANCE'];

  isloading = false;

  constructor(private router: Router, private load: LoadDataService, private  formBuilder: FormBuilder, private toastr: ToastrService) { }

  ngOnInit() {
    this.initForm();
    this.load.ishowsearch = false;
  }

  Next() {
    this.isReadyTopersonne = true;
  }
  Previous() {
    this.isReadyTopersonne = false;
  }

  initForm() {
    this.clientForm = new FormGroup({
      id: new FormControl(''),
      qui: new FormControl(''),
      importance: new FormControl(''),
      potentiel: new FormControl(''),
      nom_entreprise: new FormControl(''),
      segment: new FormControl(''),
      infogen: new FormControl(''),
      source: new FormControl(''),
      statut: new FormControl(''),
      typeclient: new FormControl(''),
      commentaire: new FormControl(''),
      nom_representant: new FormControl(''),
      prenom_representant: new FormControl(''),
      telephone_representant: new FormControl(''),
      fonction_representant: new FormControl(''),
      cp_representant: new FormControl(''),
      adresse_representant: new FormControl(''),
      ville_representant: new FormControl(''),
      pays_representant: new FormControl(''),
      date_action: new FormControl(''),
      date_creation: new FormControl(''),
    });
  }

  Submit() {
    this.isloading = true;
    const date = new Date();
    this.load.clientInsert.date_action = ''+date;
    this.load.cat.remove(this.load.clientInstance);
    this.load.cat.insert(this.load.clientInsert);
    this.clientForm.reset();
    this.isloading = false;
    this.toastr.success('Operation effectuee avec succes', 'Status');
    this.router.navigate(['/table-list']);
  }

}
