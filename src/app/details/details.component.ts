import { Component, OnInit } from '@angular/core';
import { LoadDataService } from 'app/services/load-data.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  constructor(public load:LoadDataService) { }

  ngOnInit() {
    this.load.ishowsearch = false;
  }

}
